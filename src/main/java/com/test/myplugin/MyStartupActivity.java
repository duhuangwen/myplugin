package com.test.myplugin;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.startup.StartupActivity;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description
 * @author Dwighten
 * @date 2024/8/27
 */
public class MyStartupActivity implements StartupActivity.DumbAware {

    private static final Logger log = LoggerFactory.getLogger(MyStartupActivity.class);

    @Override
    public void runActivity(@NotNull Project project) {
        log.info("Myplugin插件启动{},当前用户为{}", System.currentTimeMillis(), System.getenv("user.name"));
    }
}
