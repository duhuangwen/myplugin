package com.test.myplugin.message;

import com.intellij.DynamicBundle;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.PropertyKey;

/**
 * @description
 * @author Dwighten
 * @date 2024/8/27
 */
public class MessageBundle extends DynamicBundle {

    private static final String BUNDLE = "message.MessageBundle";
    private static final MessageBundle INSTANCE = new MessageBundle();

    private MessageBundle() {
        super(BUNDLE);
    }

    public static String message(@PropertyKey(resourceBundle = BUNDLE) String key, @NotNull Object... params) {
        return INSTANCE.messageOrNull(key, params);
    }

    public static String message(@PropertyKey(resourceBundle = BUNDLE) String key) {
        return INSTANCE.getMessage(key);
    }
}
