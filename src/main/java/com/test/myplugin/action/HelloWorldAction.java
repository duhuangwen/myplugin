package com.test.myplugin.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.ui.Messages;
import com.test.myplugin.message.MessageBundle;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorldAction extends AnAction {

    private static final Logger log = LoggerFactory.getLogger(HelloWorldAction.class);

    @Override
    public void actionPerformed(AnActionEvent e) {
        val message = MessageBundle.message("test.title");
        log.info("获取到配置文件的数据{}", message);
        String name = Messages.showInputDialog(e.getProject(), "What is your name?", "Input Your Name", Messages.getQuestionIcon());
        if (name != null && !name.trim().isEmpty()) {
            Messages.showMessageDialog(e.getProject(), "Hello " + name + "!", "Greeting", Messages.getInformationIcon());
            // Save name to a local file for future use saveNameToFile(name);
        }
    }

}