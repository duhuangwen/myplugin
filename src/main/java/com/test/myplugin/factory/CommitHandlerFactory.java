package com.test.myplugin.factory;

import com.intellij.openapi.vcs.CheckinProjectPanel;
import com.intellij.openapi.vcs.changes.CommitContext;
import com.intellij.openapi.vcs.checkin.CheckinHandler;
import com.intellij.openapi.vcs.checkin.CheckinHandlerFactory;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @description
 * @author Dwighten
 * @date 2024/8/27
 */
public class CommitHandlerFactory extends CheckinHandlerFactory {

    private static final Logger log = LoggerFactory.getLogger(CommitHandlerFactory.class);

    @Override
    public @NotNull CheckinHandler createHandler(@NotNull CheckinProjectPanel panel, @NotNull CommitContext commitContext) {
        return new MyCommitHandler(panel);
    }


    private static class MyCommitHandler extends CheckinHandler {
        private final CheckinProjectPanel panel;

        public MyCommitHandler(CheckinProjectPanel panel) {
            this.panel = panel;
        }

        @Override
        public void checkinSuccessful() {
            val project = panel.getProject();
            val commitMessage = panel.getCommitMessage();
            val selectedChanges = panel.getSelectedChanges();
            System.out.println();
            log.info("提交成功，项目名是{}", project.getName());
            log.info("提交成功，提交信息是{}", commitMessage);
            log.info("提交成功，提交文件数为{}", selectedChanges.size());
        }

    }
}
