pluginManagement {
    repositories {
        maven {
            // 阿里云公共仓库
            setUrl("https://maven.aliyun.com/repository/public")
            // 阿里云 Gradle 插件仓库
            setUrl("https://maven.aliyun.com/repository/gradle-plugin")
        }
    }
}

rootProject.name = "myplugin"