plugins {
    id("java")
    id("org.jetbrains.intellij") version "1.15.0"
}

group = "com.test"
version = "1.0-SNAPSHOT"

repositories {
    maven {
        // 阿里云公共仓库
        setUrl("https://maven.aliyun.com/repository/public")
        // 阿里云 Gradle 插件仓库
        setUrl("https://maven.aliyun.com/repository/gradle-plugin")
    }
}

// Configure Gradle IntelliJ Plugin
// Read more: https://plugins.jetbrains.com/docs/intellij/tools-gradle-intellij-plugin.html
intellij {
    version.set("2022.2")
    type.set("IC") // Target IDE Platform
    pluginName.set("myplugin")
    plugins.set(listOf("java"))
}

dependencies {
    implementation("cn.hutool:hutool-all:5.8.16")
    implementation("com.squareup.okhttp3:okhttp:4.12.0")
    implementation("org.slf4j:slf4j-api:2.0.7")
    implementation("org.slf4j:slf4j-simple:2.0.7")
    compileOnly("org.projectlombok:lombok:1.18.30")
    annotationProcessor("org.projectlombok:lombok:1.18.30")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks {
    intellij {
        updateSinceUntilBuild.set(false)
        downloadSources.set(false)
    }

    signPlugin {
        certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
        privateKey.set(System.getenv("PRIVATE_KEY"))
        password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
    }

    publishPlugin {
        token.set(System.getenv("PUBLISH_TOKEN"))
    }
}
